package com.robertoman.sproxy.util;

public class Constants {
    public static final String SECURITY_CONSTRAINT_CONFIDENTIAL = "CONFIDENTIAL";

    public static final String INDEX_HTML = "<!DOCTYPE html><html style=\"overflow-x: hidden;\"><head><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width,initial-scale=1\"/><title>SProxy</title></head><body style=\"background-color: black;\"><h1 style=\"text-align: center;width: 100%;position: absolute;top: 50%; color: white;\"><span style=\"color: green\">SP</span><span style=\"color: white\">RO</span><span style=\"color: red\">XY</span></h1></body></html>";
}
